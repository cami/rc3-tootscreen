# WARNING

Do not use this project on a public server as all dependencies are hardcoded. Might there are outdated versions of the different libraries. 

# Mastodon Timeline Viewer

## Original text
Peek into the local timeline of Mastodon instances.

ToDo
- more stats (posts per day etc)

## This version

Please have a look at the original project to see how it works. The original project can be found here: https://mastovue.glitch.me/#/
The source code from the original project can be found here: https://glitch.com/edit/#!/mastovue

## My changes
- I downloaded all the different css and script things so that only localhost will serve them
- Changed some colors so that it fits more into rc3 (it's still bad, I'm not a designer) 
- Removed some stuff so that it fits better on a separate monitor (with this version you will not have any buttons and so on, sorry)
- Made the font smaller


### Usage

I used it the following way but feel free to use it in any way you'd like.

```bash
git clone https://codeberg.org/cami/rc3-tootscreen
cd rc3-tootscreen
python3 -m http.server 
```

Open your favorite web browser and open an URL like this: ` http://localhost:8000/#/<MASTODON_SERVER)/federated/<HASHTAG>` 

Remember that you'll have to adjust the MASTODON_SERVER and the HASHTAG in the URL.

To update the displayed things I'm using a Firefox Extension called [Tab Reloader](https://addons.mozilla.org/en-GB/firefox/addon/tab-reloader/)


## License
The License of this project can be found [here](LICENSE.md)
